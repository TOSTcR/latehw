// TASK 1

const clients1 = [
	'Гилберт',
	'Сальваторе',
	'Пирс',
	'Соммерс',
	'Форбс',
	'Донован',
	'Беннет',
];
const clients2 = ['Пирс', 'Зальцман', 'Сальваторе', 'Майклсон'];

const allClients = [
	...clients1,
	...clients2.filter(c => !clients1.includes(c)),
];

// TASK 2

const characters = [
	{
		name: 'Елена',
		lastName: 'Гилберт',
		age: 17,
		gender: 'woman',
		status: 'human',
	},
	{
		name: 'Кэролайн',
		lastName: 'Форбс',
		age: 17,
		gender: 'woman',
		status: 'human',
	},
	{
		name: 'Аларик',
		lastName: 'Зальцман',
		age: 31,
		gender: 'man',
		status: 'human',
	},
	{
		name: 'Дэймон',
		lastName: 'Сальваторе',
		age: 156,
		gender: 'man',
		status: 'vampire',
	},
	{
		name: 'Ребекка',
		lastName: 'Майклсон',
		age: 1089,
		gender: 'woman',
		status: 'vempire',
	},
	{
		name: 'Клаус',
		lastName: 'Майклсон',
		age: 1093,
		gender: 'man',
		status: 'vampire',
	},
];

const arr = characters.map(f => ({
	name: f.name,
	lastName: f.lastName,
	age: f.age,
}));
console.log(arr);

// TASK 3

const user1 = {
	name: 'John',
	years: 30,
};

const { name, years, isAdmin: isAdmin = false } = user1;
const root = document.querySelector('#root');

for (let key in user1) {
	let string = document.createElement('p');
	string.innerText = `${key} : ${user1[key]} ;`;
	root.append(string);
}

// TASK 4

const satoshi2020 = {
	name: 'Nick',
	surname: 'Sabo',
	age: 51,
	country: 'Japan',
	birth: '1979-08-21',
	location: {
		lat: 38.869422,
		lng: 139.876632,
	},
};

const satoshi2019 = {
	name: 'Dorian',
	surname: 'Nakamoto',
	age: 44,
	hidden: true,
	country: 'USA',
	wallet: '1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa',
	browser: 'Chrome',
};

const satoshi2018 = {
	name: 'Satoshi',
	surname: 'Nakamoto',
	technology: 'Bitcoin',
	country: 'Japan',
	browser: 'Tor',
	birth: '1975-04-05',
};

function foo(obj1, obj2) {
	let proxy1 = JSON.parse(JSON.stringify(obj1));
	let proxy2 = JSON.parse(JSON.stringify(obj2));

	for (let key1 in proxy1) {
		for (let key2 in proxy2) {
			if (key1 == key2) {
				proxy1[key1] = proxy2[key2];
			} else {
				proxy1[key2] = proxy2[key2];
			}
		}
	}
	return proxy1;
}
let firstSearch = foo(satoshi2018, satoshi2019);
let secondSearch = foo(firstSearch, satoshi2020);

// TASK 5

const books = [
	{
		name: 'Harry Potter',
		author: 'J.K. Rowling',
	},
	{
		name: 'Lord of the rings',
		author: 'J.R.R. Tolkien',
	},
	{
		name: 'The witcher',
		author: 'Andrzej Sapkowski',
	},
];

const AddedBook = {
	name: 'Game of thrones',
	author: 'George R. R. Martin',
};

function addBook(shelf, book) {
	let objShelf = JSON.parse(JSON.stringify(shelf));
	let objBook = JSON.parse(JSON.stringify(book));

	objShelf.push(objBook);
}
addBook(books, AddedBook);

// TASK 6

const employee = {
	name: 'Vitalii',
	surname: 'Klichko',
};
const newEmployee = {};
Object.assign(newEmployee, employee);

newEmployee['age'] = 51;
console.log(newEmployee);

// TASK 7

const arr2 = ['value', () => 'showValue'];
console.log(arr2);

const [value, showValue] = arr2;

alert(value);
alert(showValue);
